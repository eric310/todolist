@extends('layouts.main')

@section('content')

	<h1> Votre liste des tâches <small>(<a href="{{ URL::route('home') }}">New Task</a></small>) </h1>

	<ul>

		@foreach ($items as $item)
			<li>
				{{ Form::open() }}
					<input 
						type="checkbox" 
						name="id" 
						onclick="this.form.submit()" 
						value="{{ $item->id }}" 
						{{ $item->done ? 'checked' :'' }} 
					/>

					<input type ="hidden" name="id" value="{{ $item->id }}" />

					{{ $item->name }} <small>(<a href="{{ URL::route('delete', $item->id) }}">x</a></small>

				{{ Form::close() }}
			</li>

		@endforeach

	</ul>

@stop