<!DOCTYPE html>
<html>

<head>

	<title>Todo List Application</title>
	<link rel="stylesheet" href="{{ URL::asset('css/style.css') }}" />
</head>

<body>

	<div class="container">

		@yield('content')
		
	</div>

</body>

</html>