@extends('layouts.main')

@section('content')
	<h1> Créer une nouvelle tâche </h1>

	@foreach ($errors->all() as $error)
		<p class="error">{{ $error }}</p>

	@endforeach

	{{ Form::open() }}
		<input type="text" name="name" placeholder="Le nom de votre tâche" />
		<input type="submit" value="Créer" />

	{{ Form::close() }}

@stop