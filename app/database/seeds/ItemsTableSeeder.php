<?php

class ItemsTableSeeder extends Seeder {
	public function run(){
		DB::table('items')->delete();

		$items = array (
			array(
				'owner_id' => 1,
				'name' => 'Acheter du lait',
				'done' => false,
			),

			array(
				'owner_id' => 1,
				'name' => 'Promener le chien',
				'done' => true,
			),

			array(
				'owner_id' => 1,
				'name' => 'Préparer le repas',
				'done' => false,
			),
		);

		DB::table('items')->insert($items);

	}
}